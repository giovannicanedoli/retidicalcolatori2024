#include <stdio.h>
#include <stdlib.h>
#include "database.h"


void print_persona(Persona* pers){
    if(pers == NULL){
        printf("[PRINT_PERSONA] NOT FOUND!\n");
        return;
    }
    printf("%s %s %s %d\n", pers -> name, pers -> surname, pers -> address, pers -> age);
    return;
}

int main(){
    Database* db = malloc(sizeof(Database));
    
    Persona p = {"Giovanni\0","Canedoli\0", "Roma\0", 21};
    Persona p2 = {"Mario\0", "Rossi\0", "Rieti\0", 10};
    Persona p3 = {"Luca\0", "Gepp\0", "Lucca\0", 30};

    insert(db, &p);
    

    Persona* answer;
    
    answer = findByName(db,"Giovanni");

    print_persona(answer);
    printf("\n\n");


}