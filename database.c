#include "database.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void insert_tree_char(IndexNodeString** root, char* string, Persona* person){
    
    if((*root)== NULL){
        *root = malloc(sizeof(IndexNodeString));
        (*root) -> value = malloc(strlen(string));
        strcpy((*root) -> value, string);

        (*root) -> person = malloc(sizeof(Persona));
        
        (*root) -> person -> name = malloc(strlen(person -> name));
        (*root) -> person -> surname = malloc(strlen(person -> address));
        (*root) -> person -> address = malloc(strlen(person -> address));


        strcpy((*root) -> person -> name, string);
        strcpy((*root) -> person -> surname, person -> surname);
        strcpy((*root) -> person -> address, person -> address);
        (*root) -> person -> age = person -> age;
        (*root) -> left = NULL;
        (*root) -> right = NULL;
        (*root) -> value = string;
        (*root) -> person = person;
        return;
    }else if(strcmp((*root) -> value, person -> name) < 0){
        if((*root) -> left == NULL){
            (*root) -> left = malloc(sizeof(IndexNodeString));
            (*root) -> value = string;
            (*root) ->left-> left = NULL;
            (*root) ->left-> right = NULL;
            (*root) -> person = person;
            return;
        }else{
            insert_tree_char(&((*root) -> left), string, person);
            return;
        }
    }else{
        if((*root) -> right == NULL){
            (*root) -> right = malloc(sizeof(IndexNodeString));
            (*root) -> value = string;
            (*root) ->right-> left = NULL;
            (*root) -> right->right = NULL;
            (*root) -> person = person;
            return;
        }else{
            insert_tree_char(&((*root) -> right), string, person);
            return;
        }
    }

}

void insert_tree_int(IndexNodeInt** root, int value, Persona* person){
    
    if(*root == NULL){
        *root = malloc(sizeof(IndexNodeInt));
        (*root) -> value = value;

        (*root) -> person = malloc(sizeof(Persona));

        (*root) -> person -> name = malloc(strlen(person -> name));
        (*root) -> person -> surname = malloc(strlen(person -> address));
        (*root) -> person -> address = malloc(strlen(person -> address));

        strcpy((*root) -> person -> name, person -> name);
        strcpy((*root) -> person -> surname, person -> surname);
        strcpy((*root) -> person -> address, person -> address);
        (*root) -> person -> age = person -> age;
        (*root) -> left = NULL;
        (*root) -> right = NULL;
        return;
    }else if((*root) -> value < value){
        if((*root) -> left == NULL){
            (*root) -> left = malloc(sizeof(IndexNodeInt));
            (*root) -> value = value;
            (*root) ->left-> left = NULL;
            (*root) ->left-> right = NULL;
            (*root) -> person = person;
            return;
        }else{
            insert_tree_int(&((*root) -> left), value, person);
            return;
        }
    }else{
        if((*root) -> right == NULL){
            (*root) -> right = malloc(sizeof(IndexNodeInt));
            (*root) -> value = value;
            (*root) ->right-> left = NULL;
            (*root) -> right->right = NULL;
            (*root) -> person = person;
            return;
        }else{
            insert_tree_int(&((*root) -> right), value, person);
            return;
        }
    }

}


void insert(Database* database, Persona* persona){
    if(database == NULL){
        perror("ERROR: DATABASE IS NULL");
        exit(EXIT_FAILURE);
    }

    if(persona == NULL){
        perror("ERROR: DATABASE IS NULL");
        exit(EXIT_FAILURE);
    }
    
    insert_tree_char(&(database->name),persona -> name, persona);
    insert_tree_char(&(database->surname), persona -> surname, persona);
    insert_tree_char(&(database->address), persona -> address, persona);
    insert_tree_int(&(database->age), persona -> age, persona);
    return;
}


Persona* find_char_value(IndexNodeString* root, char* name){
    if(root == NULL){
        return NULL;
    }

    if(name == NULL){
        perror("NAME IS NULL!");
        exit(EXIT_FAILURE);
    }

    if(!strcmp(name,root -> value)){
        return root -> person;
    }

    if(strcmp(root -> value, name) > 0){
        find_char_value(root -> left, name);
    }else{
        find_char_value(root -> right, name);
    }
}

Persona* find_int_value(IndexNodeInt* root, int age){
    if(root == NULL){

        return NULL;
    }
    if(root->value == age){
        //found person, I return
        return root -> person;
    }
    if(root -> value < age){
        find_int_value(root -> left, age);
    }
    if(root -> value > age){
        find_int_value(root -> right, age);
    }
    return NULL;
}

Persona* findByName(Database* database, char* name){
    if(database == NULL){
        perror("ERROR: DATABASE PROVIDED IS NULL");
        return NULL;
    }

    IndexNodeString* root = database -> name;
    return find_char_value(root, name);

}

Persona* findBySurname(Database * database, char * surname){
    if(database == NULL){
        perror("ERROR: DATABASE PROVIDED IS NULL");
        return NULL;
    }
    IndexNodeString* root = database -> surname;

    return find_char_value(root, surname);
}

Persona* findByAddress(Database * database, char * address){
    if(database == NULL){
        perror("ERROR: DATABASE PROVIDED IS NULL");
        return NULL;
    }
    IndexNodeString* root = database -> address;

    return find_char_value(root, address);
}

Persona* findByAge(Database * database, int age){

    if(database == NULL){
        perror("ERROR: DATABASE PROVIDED IS NULL");
        return NULL;
    }

    IndexNodeInt* root = database -> age;

    return find_int_value(root, age);
}
